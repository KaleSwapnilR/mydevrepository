<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Out_Of_office_notification</fullName>
        <description>Out Of office notification</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_user_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>VSTrackIT/Out_of_office_notification</template>
    </alerts>
    <rules>
        <fullName>Out of office notification</fullName>
        <actions>
            <name>Out_Of_office_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_assigned_records__c.Assigned_user_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This works flow sends an out of office notification</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
