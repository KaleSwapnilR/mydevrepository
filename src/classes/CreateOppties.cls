public with sharing class CreateOppties {

    public Integer parameterIndex{get;set;} //Gets Parameter from the page so that dependent picklist values can be changed.
    public Integer counter{get;set;}        //to count number of rules added.
    public List<OpportunityRollOverMapping> listMapping {get;set;} // gets Mapping list of rules.
    
    //Constructor to initialize the variables.
    public CreateOppties()
    {
        OpportunityRollOverMapping opportunityRollOverMapping = new OpportunityRollOverMapping();
        this.listMapping = new List<OpportunityRollOverMapping>();
        counter = 0;
        opportunityRollOverMapping.counterWrap= 0 ;

    }
    //Gets called form the Vf page
    //Change the Probability Stage Mapping List depending on the field name.
    public PageReference showselected() {
        System.debug('**listMapping'+listMapping ); 
        System.debug('**CounterWrapparameterIndex'+parameterIndex);
        for(Integer i=0;i<listMapping.size();i++)
        {
            if(listMapping[i].counterWrap == parameterIndex )
            {
                if(listMapping[i].fieldName == 'Stage')
                {
                    listMapping[i].probabilityStageMappingList = getOpptyStages();
                }
                else if(listMapping[i].fieldName == 'Probability')
                {
                    listMapping[i].probabilityStageMappingList =  getOpptyProbability();
                }

            }
        } 
        return Null;
    }
    //Add the Mapping Rows i.e Rules
    public PageReference addMappingRow()
    {
        List<SelectOption> listOfProbabilityStageMapping = new List<SelectOption>();
        listOfProbabilityStageMapping.add(new SelectOption('<Select>','<Select>'));
        OpportunityRollOverMapping opportunityRollOverMapping = new OpportunityRollOverMapping();
        counter++;
        opportunityRollOverMapping.counterWrap= counter;
        opportunityRollOverMapping.fieldName = '<Select>';
        opportunityRollOverMapping.fromProbRangeOrStage ='<Select>';
        opportunityRollOverMapping.toStage='<Select>';
        opportunityRollOverMapping.probabilityStageMappingList =listOfProbabilityStageMapping;
        listMapping.add(opportunityRollOverMapping);
        return null;
    }
    //Remove the rule added to addMappingRow
    public pageReference removeMappingRow()
    {
        listMapping.remove(1);
        counter--;
        return Null;
    }

   //Wrapper class 
    public class OpportunityRollOverMapping{
        Public Integer counterWrap{get;set;}
        public String fieldName{get;set;}
        public String fromProbRangeOrStage{get;set;}
        public String toStage{get;set;}
        public List<SelectOption> probabilityStageMappingList{get;set;}

    }

        public List<selectoption> getOpptyStages() {
        List<selectoption> options = new List<selectoption>();
        //This is some generic code to retrieve the current stage names using
        //  Dynamic Apex in case the Stage Names change in the future
        Schema.DescribeFieldResult F = Opportunity.StageName.getDescribe();
        List<schema.picklistentry> typeList = F.getPicklistValues();

        for (Schema.PicklistEntry TypeItem : typeList){
            options.add(new SelectOption(TypeItem.getValue(),TypeItem.getLabel()));
        }

        return options; 
    }

    public List<SelectOption> getOpptyProbability()
    {
        List<SelectOption> listOfProbability = new List<SelectOption>();
        listOfProbability.Clear();
        listOfProbability.add(new SelectOption('0%','0%'));
        listOfProbability.add(new SelectOption('1% to 10%','1% to 10%'));
        listOfProbability.add(new SelectOption('11% to 25%','11% to 25%'));
        listOfProbability.add(new SelectOption('26% to 50%','26% to 50%'));
        listOfProbability.add(new SelectOption('51% to 75%','51% to 75%'));
        listOfProbability.add(new SelectOption('76% to 89%','76% to 89%'));
        listOfProbability.add(new SelectOption('90% to 99%','90% to 99%'));
        listOfProbability.add(new SelectOption('100%','100%'));
        return listOfProbability;
    }   

    public List<SelectOption> getCategories() {
        List<SelectOption> listOfOpptyFields = new List<SelectOption>();
        listOfOpptyFields.add(new SelectOption('<Select>','<Select>'));
        listOfOpptyFields.add(new SelectOption('Stage','Stage'));
        listOfOpptyFields.add(new SelectOption('Probability','Probability'));
        return listOfOpptyFields;
    }    
    public PageReference Save() {
        System.debug('**--listMapping'+listMapping );
        System.debug('**--listMapping Size'+listMapping.size() );
        return null;
    }
}