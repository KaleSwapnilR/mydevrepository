public class DependentObjects {

    public List<SelectOption> getCategories() {
        List<SelectOption> listOfOpptyFields = new List<SelectOption>();
        listOfOpptyFields.add(new SelectOption('Probability','Probability'));
        listOfOpptyFields.add(new SelectOption('Stage','Stage'));
        return listOfOpptyFields;
    }


    public List<SelectOption> getFeatures() {
    
        List<SelectOption> listOfProbability = new List<SelectOption>();
        
        if(category == 'Probability')
        {
        listOfProbability.add(new SelectOption('0%','0%'));
        listOfProbability.add(new SelectOption('1% to 10%','1% to 10%'));
        listOfProbability.add(new SelectOption('11% to 25%','11% to 25%'));
        listOfProbability.add(new SelectOption('26% to 50%','26% to 50%'));
        listOfProbability.add(new SelectOption('51% to 75%','51% to 75%'));
        listOfProbability.add(new SelectOption('76% to 89%','76% to 89%'));
        listOfProbability.add(new SelectOption('90% to 99%','90% to 99%'));
        listOfProbability.add(new SelectOption('90% to 99%','100%')); 
        }
        else
        {
        
        listOfProbability.add(new SelectOption('abc','Abc'));
        listOfProbability.add(new SelectOption('Xyx','xyz'));
        listOfProbability.add(new SelectOption('lmn','lmn')); 
        
        
        }
        return listOfProbability;
    }


   
    /* String value for the category */
    String category;
    /* String value for the feature */
    String feature;
 
    /* Getter for the category value */
    public String getCategory() { return this.category; }
 
    /* Setter for the category value */
    public void setCategory(String s) { this.category = s; }
 
    /* Getter for the feature value */
    public String getFeature() { return this.feature; }
  
    /* Setter for the feature value */
    public void setFeature(String s) { this.feature = s; }
}