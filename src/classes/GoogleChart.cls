public class GoogleChart{
    public Decimal intWon {get; set;}
    public Decimal intLost {get; set;}
    public Decimal intGates0 {get; set;}
    public Decimal intGates1 {get; set;}
    public Decimal intGates2 {get; set;}
    public Decimal intGates3 {get; set;}
    public Decimal intGates4 {get; set;}
    public Decimal intGates5 {get; set;}
    public Decimal intGates6 {get; set;}
    public Decimal intNego {get; set;} 
    public Decimal intMillionForm {get; set;}
    public list<AggregateResult> lstopp{get; set;} 
    public GoogleChart(){
      lstopp = [select count(id) total,stagename from Opportunity group by stagename];
      for(AggregateResult ar: lstopp){
            if(ar.get('stagename') == 'Closed Won') 
            {
             intWon = integer.valueOf(ar.get('total'));
            } 
            if(ar.get('stagename') == 'Closed Lost')
            {
             intLost = integer.valueOf(ar.get('total'));
            } 
            if(ar.get('stagename') == 'Prospecting')
            {
              intGates0 = integer.valueOf(ar.get('total'));
            }    
            if(ar.get('stagename') == 'Qualification')
            {
              intGates1 = integer.valueOf(ar.get('total'));
            }     
            if(ar.get('stagename') == 'Needs Analysis')
            {
              intGates2 = integer.valueOf(ar.get('total'));
            }     
            if(ar.get('stagename') == 'Value Proposition')
            {
              intGates3 = integer.valueOf(ar.get('total'));
            }   
            if(ar.get('stagename') == 'Id. Decision Makers')
            {
              intGates4 = integer.valueOf(ar.get('total'));
            } 
            if((ar.get('stagename') == 'Perception Analysis'))
            {
              intGates5 = integer.valueOf(ar.get('total'));
            }
            if((ar.get('stagename') == 'Proposal/Price Quote'))
            {
              intGates6 = integer.valueOf(ar.get('total'));
            }
            if((ar.get('stagename') == 'Negotiation/Review'))
            {
              intNego = integer.valueOf(ar.get('total'));
            }                        
       }
    } 
 }