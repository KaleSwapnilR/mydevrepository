public class IntrestOfCandidateControler {
 Public lead lObj;
 Public Id leadId;
 
   public IntrestOfCandidateControler() {
        leadId = ApexPages.currentPage().getParameters().get('id');
        lObj = (leadid == null) ? new Lead():[SELECT Name from lead where id =: leadid];
        
    }
    
     public void leadtoOpportunity()
     {
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(leadId);
        LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());
        Id oppId = lcr.getOpportunityId();
        
     }
 
}