/****************************************************************************************
 * Author: Swapnil Kale swapnil.kale@fantaitech.com
 * Developed on : 6 Aug 2014
 * Opportunities will be inserted from past show according to specified rule Criteria in current show. 
 * It will get all opportunity from past show with matching crieria rules and insert these 
 * opportunity with its updated stage from rule. 
 ****************************************************************************************/

public with sharing class OppotunitiesCreationWizardController{

    public Integer parameterIndex{get;set;} //Gets Parameter from the page so that dependent picklist values can be changed.
    public Integer counter{get;set;}        //to count number of rules added.
    public List<OpportunityRollOverMapping> listMapping {get;set;} // gets Mapping list of rules.
    public string currentShow{get;set;}    //get current show value.
    public Opportunity oppty{get;set;}     //get opportunity.
    public string pastShow{get;set;}        //get past show.
    List<Opportunity> opportunityWithPastShow{get;set;}   //get all the opprtunities related to past show

    public string pastShowError{get;set;}    //If past show picklist contains <Select> 
    public string currentShowError{get;set;} 
    public String mappingError{ get; set; }  //If no mapping is selected give an error.   

    public List<OpportunityDataToShowPreview> opportunityDataToShowPreview {get;set;} // list to show details on vf page it contains  Total Opportunities,Total Amount And Total Sales Pipeline 
    public String successMessage {get;set;}
    public String closeDateError {get;set;}
    public String ownerError {get;set;}
    
    public Boolean showPreview {get;set;}        //Show preview if user hits on preview buton;

    Map<Decimal,String> mapProbabilityWithRange ;  
    Map<String,String> mapFromAndToCriteria;
    //Constructor to initialize the variables.
    public OppotunitiesCreationWizardController()
    {
        oppty=new Opportunity(); 
        this.listMapping = new List<OpportunityRollOverMapping>();
        counter = 0;
        opportunityWithPastShow = new List<Opportunity>();
        addMappingRow() ;
        opportunityDataToShowPreview = new List<OpportunityDataToShowPreview>();
        successMessage = '';
        getAllMapsValue();
    }
    //Create Opportunity on click of "Create Opportunity" button on vf page. 
    public PageReference createOpportunities() {
        showPreview = false ;
        if(validate() == True)
        {
            opportunityWithPastShow  = getOpportunitiesAccordingToRules();
            if(opportunityWithPastShow.size()>0 && opportunityWithPastShow!=Null )
            {
                pastShowError='';
                insertOpportunity(opportunityWithPastShow );
                successMessage = 'Opportunities have been created successfully. Total Opportunities created = ' +opportunityWithPastShow.size();
            }
            else
            {
                pastShowError='There are no opportunities for this \n show in the selected range of probabilities';
            }

        }  
        return null;
    }

    //get all opportunities to show preview of how many opportunities will be created for each stage,
    // total quantity, and amount, and total pipeline amount.           
    public PageReference previewOpportunities() {
        opportunityDataToShowPreview.clear();
        if(validate() == True)
        {
            opportunityWithPastShow  = getOpportunitiesAccordingToRules();

            if(opportunityWithPastShow.size()>0 && opportunityWithPastShow!=Null )
            {
                pastShowError='';
                List<AggregateResult> aggResult= [SELECT COUNT(id) totalCount , SUM(Amount) totalAmount , AVG(Probability) totalProbability ,StageName  FROM Opportunity where id in: opportunityWithPastShow group by StageName];

                for(AggregateResult a : aggResult) 
                {
                    opportunityDataToShowPreview.add(new opportunityDataToShowPreview(Integer.valueOf(a .get('totalCount')),(decimal)a.get('totalAmount'),(decimal)a.get('totalAmount')/Integer.valueOf(a .get('totalCount')),String.valueOf(a .get('StageName'))  ));
                }
                showPreview = true ;
            }
            else
            {
                pastShowError='There are no opportunities for this \n show in the selected probability / stage mapping criteria';
                showPreview = false ;
            }
        }        

        return null; 
    }

    //Initialize values of maps.
    void getAllMapsValue()
    {

        mapFromAndToCriteria = new Map<String,String>();
        for(OpportunityRollOverMapping  opp : ListMapping)
        {
            mapFromAndToCriteria.put(opp.fromProbRangeOrStage,opp.toStage);
        }
        system.debug('**mapFromAndToCriteria '+mapFromAndToCriteria );

        mapProbabilityWithRange = new Map<Decimal,String>();
        for(integer i=0 ;i<=100;i++)
        {
            if(i==0)
            {
                mapProbabilityWithRange.put(i,'0%');
            }
            else if(i>=1 && i<=10)
            {
                mapProbabilityWithRange.put(i,'1% to 10%');
            }
            else if(i>=11 && i<=25)
            {
                mapProbabilityWithRange.put(i,'11% to 25%');
            } 
            else if(i>=26 && i<=50)
            {
                mapProbabilityWithRange.put(i,'26% to 50%');
            }
            else if(i>=51 && i<=75)
            {
                mapProbabilityWithRange.put(i,'51% to 75%');
            }
            else if(i>=76 && i<=89)
            {
                mapProbabilityWithRange.put(i,'76% to 89%');
            }
            else if(i>=90 && i<=99)
            {
                mapProbabilityWithRange.put(i,'90% to 99%');
            }
            else if(i==100)
            {
                mapProbabilityWithRange.put(i,'100%');
            }
        }
        system.debug('*mapProbabilityWithRange *'+mapProbabilityWithRange );

    }
    //valiadates all the required fields
    Boolean validate()
    {
        currentShowError= '' ;
        pastShowError = '';
        mappingError = '';
        closeDateError='';
        ownerError ='';
        Integer i =0 ,j=0;

        if(pastShow==currentShow)
        {
            currentShowError='Please select diffrent show';
            return false ;
        }
        if(pastShow=='<Select>')
        {
            pastShowError='Please select past show';
            return false ;
        }

        if(currentShow=='<Select>')
        {    
            currentShowError='Please select current show';
            return false ;
        } 
        if(oppty.CloseDate==Null)
        {    
            closeDateError ='Please select close date';
            return false ;
        } 
        if(oppty.OwnerId==Null)
        {    
            ownerError ='Please select substitute owner for inactive users';
            return false ;
        }

        //Check for duplicate rules on page. 
        for( i=0 ; i < ListMapping.size()-1;i++ )
        {
            if(ListMapping[i].fieldName == '<Select>' || ListMapping[i].fromProbRangeOrStage == '<Select>')
            {
                mappingError='Please select probability/stage mappings';
                return false;
            }
            for( j=i+1;j < ListMapping.size();j++)
            {
                if(ListMapping[i].fieldName == ListMapping[j].fieldName && ListMapping[i].fromProbRangeOrStage == ListMapping[j].fromProbRangeOrStage)
                {
                    mappingError='Their cannot be duplicate rule';
                    return false ;
                }

            }
        } 
        if(ListMapping[i].fieldName == '<Select>' || ListMapping[i].fromProbRangeOrStage == '<Select>')
        {
            mappingError='Please select probability/stage mappings';
            return false;
        }

        return True;
    }
    //create a new opprtunity using pastShowOpportunity and insert it for current show. Update discription and stage name according to the rule and current show selected.
    void insertOpportunity(List <Opportunity> pastShowOpportunity  )
    {
        System.debug('**pastShowOpportunity  '+pastShowOpportunity  ); 
        getAllMapsValue();
        List<Opportunity> listCurrentShowOppty=new List<Opportunity>();
        List<Show__c> listPastShow =[select id,Name from Show__c where id=:pastShow];
        List<Show__c> listCurrentShow=[select id,Name from Show__c where id=:currentShow];




        for (Opportunity opportunityWithPastShow : pastShowOpportunity  )
        {
            string opptyOwner=opportunityWithPastShow.Owner.IsActive==true?opportunityWithPastShow.OwnerId:oppty.OwnerId;
            system.debug('opportunityWithPastShow.StageName'+opportunityWithPastShow.StageName);
            system.debug('opportunityWithPastShow.StageName'+mapFromAndToCriteria.containsKey(opportunityWithPastShow.StageName));
            
            if(mapFromAndToCriteria.containsKey(opportunityWithPastShow.StageName)==true)
            {
                System.debug('**iniffor stage');                
                Opportunity currentShowOppty=new Opportunity (Name=opportunityWithPastShow.Name,CloseDate=oppty.CloseDate,Amount=opportunityWithPastShow.Amount,Probability=opportunityWithPastShow.Probability,
                        AccountId=opportunityWithPastShow.AccountId,
                        TotalOpportunityQuantity=opportunityWithPastShow.TotalOpportunityQuantity,OwnerId=opptyOwner,
                        Description ='Rolled over from '+listPastShow.get(0).Name,
                        StageName=mapFromAndToCriteria.get(opportunityWithPastShow.StageName) ,
                        Show__c=listCurrentShow.get(0).Id);
                listCurrentShowOppty.add(currentShowOppty);
            } else
                if(mapFromAndToCriteria.containsKey(mapProbabilityWithRange.get(opportunityWithPastShow.Probability)) == true)
                {
                    System.debug('**iniffor prob');                             
                    Opportunity currentShowOppty=new Opportunity (Name=opportunityWithPastShow.Name,CloseDate=oppty.CloseDate,Amount=opportunityWithPastShow.Amount,Probability=opportunityWithPastShow.Probability,
                            AccountId=opportunityWithPastShow.AccountId,
                            TotalOpportunityQuantity=opportunityWithPastShow.TotalOpportunityQuantity,OwnerId=opptyOwner,
                            Description ='Rolled over from '+listPastShow.get(0).Name,
                            StageName=mapFromAndToCriteria.get(mapProbabilityWithRange.get(opportunityWithPastShow.Probability)) ,
                            Show__c=listCurrentShow.get(0).Id);
                    listCurrentShowOppty.add(currentShowOppty);          
                }
        }        


        System.debug('**listCurrentShowOppty'+listCurrentShowOppty);  
        insert listCurrentShowOppty ;
    }
    // get all the rules in a string format first and then get all opportunity according to the rule or criteria. 
    List<Opportunity> getOpportunitiesAccordingToRules()
    {

        String stageOrProbabilityQuery = 'select Id,Name,Amount,CloseDate,AccountId,StageName,Probability,Item_Type__c,TotalOpportunityQuantity,OwnerId,Owner.IsActive from Opportunity where  (' ; 
        System.debug('**listMapping'+listMapping ); 
        List<Show__c> listPastShow =[select id,Name from Show__c where id=:pastShow];
        List<Show__c> listCurrentShow=[select id,Name from Show__c where id=:currentShow];

        if(listPastShow.size()>0 && listCurrentShow.size()>0 )
        {
            System.debug('**inif'); 
            If(listMapping.size()>0)
            {
                mappingError='';
                System.debug('**inif2'); 
                for(OpportunityRollOverMapping  opportunityRollOverMapping : ListMapping)
                {
                    if(opportunityRollOverMapping.fieldName == 'Stage')
                    {
                        stageOrProbabilityQuery = stageOrProbabilityQuery  + ' StageName = ' + '\'' +opportunityRollOverMapping.fromProbRangeOrStage  + '\'' +' or '  ;
                    }  

                    if(opportunityRollOverMapping.fieldName == 'Probability')
                    {
                        if(opportunityRollOverMapping.fromProbRangeOrStage == '0%')
                        {
                            stageOrProbabilityQuery = stageOrProbabilityQuery + '(Probability =0.0)'    +' or ';
                        }
                        else if(opportunityRollOverMapping.fromProbRangeOrStage == '1% to 10%')
                        {
                            stageOrProbabilityQuery = stageOrProbabilityQuery + '(Probability>=1.0 and Probability<=10.0)'    +' or ';
                        }
                        else if(opportunityRollOverMapping.fromProbRangeOrStage == '11% to 25%')
                        {
                            stageOrProbabilityQuery = stageOrProbabilityQuery + '(Probability>=11.0 and Probability<=25.0)'    +' or ';
                        }
                        else if(opportunityRollOverMapping.fromProbRangeOrStage == '26% to 50%')
                        {
                            stageOrProbabilityQuery = stageOrProbabilityQuery + '(Probability>=26.0 and Probability<=50.0)'    +' or ';
                        }
                        else if(opportunityRollOverMapping.fromProbRangeOrStage == '51% to 75%')
                        {
                            stageOrProbabilityQuery = stageOrProbabilityQuery + '(Probability>=51.0 and Probability<=75.0)'    +' or ';
                        }
                        else if(opportunityRollOverMapping.fromProbRangeOrStage == '76% to 89%')
                        {
                            stageOrProbabilityQuery = stageOrProbabilityQuery + '(Probability>=76.0 and Probability<=89.0)'    +' or ';
                        }
                        else if(opportunityRollOverMapping.fromProbRangeOrStage == '90% to 99%')
                        {
                            stageOrProbabilityQuery = stageOrProbabilityQuery + '(Probability>=90.0 and Probability<=99.0)'    +' or ';  
                        }
                        else if(opportunityRollOverMapping.fromProbRangeOrStage == '100%')
                        {
                            stageOrProbabilityQuery = stageOrProbabilityQuery + '(Probability=100.0)'    +' or '; 
                        }   

                    }   
                }
                stageOrProbabilityQuery = stageOrProbabilityQuery.Substring(0,stageOrProbabilityQuery.length()-3);
                stageOrProbabilityQuery = stageOrProbabilityQuery  + ' ) ' + +' and ' + 'Show__c = ' + '\'' + +listPastShow.get(0).Id + '\'' ; 
                system.debug('stageOrProbabilityQuery **'+stageOrProbabilityQuery ); 


                //stageOrProbabilityQuery = 'select Id,Name,Amount,CloseDate,AccountId,StageName,Probability,Item_Type__c,TotalOpportunityQuantity,OwnerId,Owner.IsActive from Opportunity where ' + '( StageName = \'Prospecting\' or StageName = \'Needs Analysis\' )'+' and ' + 'Show__c = ' + '\'' + +listPastShow.get(0).Id + '\'' ;

                List<Opportunity> listOfOpportunitiesFromRules= Database.query(stageOrProbabilityQuery);

                system.debug('***listOfOpportunities'+listOfOpportunitiesFromRules);  
                return listOfOpportunitiesFromRules ;                           

            }

        }
        return null;
    }


    //Gets called form the Vf page.
    //Change the Probability Stage Mapping List depending on the field name.
    public PageReference showselected() {
        System.debug('**listMapping'+listMapping ); 
        System.debug('**CounterWrapparameterIndex'+parameterIndex);
        for(Integer i=0;i<listMapping.size();i++)
        {
            if(listMapping[i].counterWrap == parameterIndex )
            {
                if(listMapping[i].fieldName == 'Stage')
                {
                    listMapping[i].probabilityStageMappingList = getOpptyStages();
                }
                else if(listMapping[i].fieldName == 'Probability')
                {
                    listMapping[i].probabilityStageMappingList =  getOpptyProbability();
                }

            }
        } 
        return Null;
    }
    //Add the Mapping Rows i.e Rules
    public PageReference addMappingRow()
    {

        List<SelectOption> listOfProbabilityStageMapping = new List<SelectOption>();
        listOfProbabilityStageMapping.add(new SelectOption('<Select>','<Select>'));
        OpportunityRollOverMapping opportunityRollOverMapping = new OpportunityRollOverMapping();
        counter++;
        opportunityRollOverMapping.counterWrap= counter;
        opportunityRollOverMapping.fieldName = '<Select>';
        opportunityRollOverMapping.fromProbRangeOrStage ='<Select>';
        opportunityRollOverMapping.toStage='<Select>';
        opportunityRollOverMapping.probabilityStageMappingList =listOfProbabilityStageMapping;
        if(listMapping.size()<8)
        {
            listMapping.add(opportunityRollOverMapping);
        }
        else
        {
            mappingError='Can not add more than 8 rules';
        }
        return null;
    }
    //Remove the rule added to addMappingRow
    public pageReference removeMappingRow()
    {
        system.debug('**listMapping'+listMapping);
        system.debug('**counter'+listMapping);
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        
        for(Integer i=0;i<listMapping.size();i++){
            if(listMapping[i].counterWrap == param ){
                listMapping.remove(i);     
            }
        }
        
        
        counter--;

        return Null;
    }

    //Wrapper class 
    public class OpportunityRollOverMapping{
        Public Integer counterWrap{get;set;}
        public String fieldName{get;set;}
        public String fromProbRangeOrStage{get;set;}
        public String toStage{get;set;}
        public List<SelectOption> probabilityStageMappingList{get;set;}

    }
    //Get Opportunity satges.
    public List<selectoption> getOpptyStages() {
        List<selectoption> options = new List<selectoption>();
        //This is some generic code to retrieve the current stage names using
        //  Dynamic Apex in case the Stage Names change in the future
        Schema.DescribeFieldResult F = Opportunity.StageName.getDescribe();
        List<schema.picklistentry> typeList = F.getPicklistValues();

        for (Schema.PicklistEntry TypeItem : typeList){
            options.add(new SelectOption(TypeItem.getValue(),TypeItem.getLabel()));
        }

        return options; 
    }
    //Get Opportunity Probability by using query .First I was hard coding the values now values are taken from the oppty stages.
    public List<SelectOption> getOpptyProbability()
    {
        List<SelectOption> listOfProbability = new List<SelectOption>();
        listOfProbability.clear();
        
        /*List<OpportunityStage> listOfProbabilityFromOpportunityStage = [Select DefaultProbability From OpportunityStage where IsActive =true ORDER BY DefaultProbability Asc ] ;
        Set<Decimal> setString = new Set<OpportunityStage>(listOfProbabilityFromOpportunityStage.DefaultProbability ); 
        
        list<String> listOfProbabitlityRange = new List<String>();
        for(OpportunityStage s : setString )
        {
           listOfProbabitlityRange.add(mapProbabilityWithRange.get(s.DefaultProbability));  
           system.debug('#setString#'+s.DefaultProbability );          
        }
        system.debug('#setString#'+listOfProbabitlityRange ); */
        
        listOfProbability.add(new SelectOption('0%','0%'));
        listOfProbability.add(new SelectOption('1% to 10%','1% to 10%'));
        listOfProbability.add(new SelectOption('11% to 25%','11% to 25%'));
        listOfProbability.add(new SelectOption('26% to 50%','26% to 50%'));
        listOfProbability.add(new SelectOption('51% to 75%','51% to 75%'));
        listOfProbability.add(new SelectOption('76% to 89%','76% to 89%'));
        listOfProbability.add(new SelectOption('90% to 99%','90% to 99%'));
        listOfProbability.add(new SelectOption('100%','100%')); 
        return listOfProbability;
    }   
    
    List<String> getSortedProbabilityRanges(List<String> probRanges)
    {
        
     return null;
    }
    //Get categories or fields 
    public List<SelectOption> getCategories() {
        List<SelectOption> listOfOpptyFields = new List<SelectOption>();
        listOfOpptyFields.add(new SelectOption('<Select>','<Select>'));
        listOfOpptyFields.add(new SelectOption('Stage','Stage'));
        listOfOpptyFields.add(new SelectOption('Probability','Probability'));
        return listOfOpptyFields;
    }    

    //Get all current show or all future shows.
    public List <SelectOption> getCurrentShows()
    {
        List<SelectOption> listOfCurrentShows=  new List<SelectOption>();
        listOfCurrentShows.add(new SelectOption('<Select>','<Select>'));   
        List<Show__C> listOfShows =[Select id,Name From Show__c ];  
        for(Show__c show:listOfShows)
            {
                listOfCurrentShows.add(new SelectOption(show.id,show.Name));
            }
        
        return listOfCurrentShows;
    } 
    //Get all past shows
    public List <SelectOption> getPastShows()
    {
        system.debug('##currentShow'+currentShow );
        List<SelectOption> listOfPastShows=  new List<SelectOption>();
        if(currentShow==Null)
        {
            listOfPastShows.add(new SelectOption('<Select>','<Select>'));
        }
        
         List<Show__C> listOfShows =[Select id,Name From Show__c];  
         for(Show__c show:listOfShows)
            {
                listOfPastShows.add(new SelectOption(show.id,show.Name));
            }
        
        return listOfPastShows;
    } 
    //class to create alist of records to show details of opportunity like Total count, Total amount, Total sales pipeline amount for every stages.
    public class OpportunityDataToShowPreview{
        public Integer totalCount { get; set; }
        public Decimal totalAmount { get; set; }
        public Decimal totalSalesPipeline {get;set;}
        public String stage {get;set;}

        public OpportunityDataToShowPreview(Integer tCount , Decimal tAmount ,Decimal tSalesPipeline,String stage ) {
            this.totalCount = tCount ;
            this.totalAmount = tAmount ;
            this.totalSalesPipeline = tSalesPipeline ;
            this.stage = stage ; 
        }
    }

}