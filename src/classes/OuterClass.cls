Public class OuterClass
{
    private static final Integer MY_INT;
    public static String sharedState;
    public static Integer getInt() { return MY_INT; }
    static {
        MY_INT = 2; 
    }
    private final String m;
    {
        m = 'a';  
    }
    public virtual interface MyInterface
    { 
    void myMethod(); 
	}
    
    interface MySecondInterface extends MyInterface { 
        Integer method2(Integer i); 
    }

    public virtual class InnerClass implements MySecondInterface {
        private final String s;
        private final String s2;
        {
            this.s = 'x';
        }
            private final Integer i = s.length();
        InnerClass() {
            this('none');
        }
        public InnerClass(String s2) { 
            this.s2 = s2; 
        }
	public virtual void myMethod() { /* does nothing */ }
    public Integer method2(Integer i) { return this.i + s.length(); }
    }
    public abstract class AbstractChildClass extends InnerClass {
	 public override void myMethod() { /* do something else */ }
        protected void method2() {}
        abstract Integer abstractMethod();
    }

    public class ConcreteChildClass extends AbstractChildClass {
        public override Integer abstractMethod() { return 5; }
    }
 
    public class AnotherChildClass extends InnerClass {
    AnotherChildClass(String s) {
      // Explicitly invoke a different super constructor than one with no arguments
      super(s);
      }
    }    
  	public virtual class MyException extends Exception {
    public Double d;
    	MyException(Double d) {
      	this.d = d;
    	}
    protected void doIt() {}
  	}
      public abstract class MySecondException extends Exception implements MyInterface {
  	
}

}