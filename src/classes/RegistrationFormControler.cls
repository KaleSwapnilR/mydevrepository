public class RegistrationFormControler {

     public String status{get;set;}
     public string errorFN{get;set;}   
     public string errorLN{get;set;}
     public string errorComp{get;set;}
     public string errorTitle{get;set;}
     public string errorEmail{get;set;}
     public string errorPhone{get;set;}
     public string errorAdd1{get;set;}
  
     public string errorCity{get;set;}
     public string errorState{get;set;}
     public string errorZip{get;set;}
     public string errorCountry{get;set;}
     public ID contactId, splId;
    public Contact contact {get;set;}
    public Contact contact1 {get;set;}
    public Account account{get;set;}
    public Specialization__c splObj {get;set;}
    public string s1;
    
    public Candidate__c candidate {get;set;}
    public Boolean insertconfrm; 
    
    
    public RegistrationFormControler(){
    contact=new Contact();
    contact1=new Contact();
    account=new Account();
    candidate = new Candidate__c();
    splObj = new Specialization__c();
    }
    
    public void dataInsert()
    {
        try{
        System.debug('after validation'+candidate.Course__c);
           List<Specialization__c> splList = [select id,Start_Date__c from Specialization__c where recordtype.name =:candidate.Course__c];
                splObj  = splList.get(0);
                splId =  splList.get(0).id;
                 System.debug('s1 List'+splList +splId+ splList);
            
        //List<RecordType> = new List<RecordType>([select id, name from recordtype where sobjecttype="MyObj__c"]);
         
            List<contact> contactList = [select ID, AccountID,Name,FirstName,LastName,Email,Phone,Fax,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry from contact where LastName=:contact.LastName and Email=:contact.Email ];
            system.debug('list##'+contactList.size());    
           
                if(contactList.size()>0)
                {
                contact=contactList.get(0);
                contactId = contactList.get(0).ID ;                    
                   /* contact1.FirstName= contact.FirstName;
                    contact1.LastName= contact.LastName;
                    contact1.Phone= contact.Phone;
                    contact1.Email= contact.Email;
                    contact1.MailingCity= contact.MailingCity;
                    contact1.MailingStreet= contact.MailingStreet;
                    contact1.MailingState= contact.MailingState;
                    contact1.MailingPostalCode= contact.MailingPostalCode;
                    contact1.MailingCountry= contact.MailingCountry;
                    update contact1; */
                    update contact;
                system.debug('Update');                        
                }
                else
                {
                system.debug('insert@@@@@');
                insert contact;
                contactId = contact.ID ;                       
                } 
                system.debug('contactID##'+contactID);    
                candidate.Contact__c = contactId;
                candidate.Date_Submited__c = system.today();
                candidate.Stream__c = splId ;           
                insert candidate; 
       
             //   insert splObj;
                
                insertconfrm = true;
                 System.debug('True');
                
        }catch(exception e)
        {
              System.debug('excep');
                insertconfrm = false;                
        }                
    }   
 public PageReference submitApply()
 {
        
     System.debug('Before validation'+candidate.Course__c);
      IF(validate()=='true')
      {
          dataInsert();
             
          if(insertconfrm == true)
          {
              System.debug('Form confrm');
             PageReference payment = new PageReference('/apex/FormConfirmation'); 
             System.debug('Before validation');
          }    
      }
         return null;
 }
 public PageReference submitMakePayment()
 {
 System.debug('Before validation'+candidate.Course__c);
      IF(validate()=='true')
      {
          dataInsert();
             
          if(insertconfrm == true)
          {
             PageReference payment = new PageReference('/apex/FormConfirmation'); 
             System.debug('Before validation');
          }    
      }
         return null;
 }
 
 
 
 
  public string validate(){
          System.debug('In Validation');
        status='true';
        if(contact.FirstName==null || contact.FirstName.length()==0){
          errorFN='Error: You must enter a Value';
            System.debug('In Validation...validating first name');
          status='false';
        }else{
          errorFN='';         
        } 
        
        if(contact.LastName==null || contact.LastName.length()==0){
          errorLN='Error: You must enter a Value';
             System.debug('In Validation...validating last name');
          status='false';
        }else{
          errorLN='';         
        } 
        
        if(contact.Email==null || contact.Email.length()==0){
          errorEmail='Error: You must enter a Value';
             System.debug('In Validation...validating email name');
          status='false';
        }else{
          errorEmail='';          
        } 
        
        if(contact.Phone==null || contact.Phone.length()==0){
          errorPhone='Error: You must enter a Value';
             System.debug('In Validation...validating phone name');
          status='false';
        }else{
          errorPhone='';          
        } 
        
        if(contact.MailingStreet==null || contact.MailingStreet.length()==0){
          errorAdd1='Error: You must enter a Value';
             System.debug('In Validation...validating MS name');
          status='false';
        }else{
          errorAdd1='';         
        } 
        
               
        if(contact.MailingCity==null || contact.MailingCity.length()==0){
          errorCity='Error: You must enter a Value';
             System.debug('In Validation...validating MC name');
          status='false';
        }else{
          errorCity='';        
        } 
        
        if(contact.MailingState==null || contact.MailingState.length()==0){
          errorState='Error: You must enter a Value';
             System.debug('In Validation...validating MS name');
          status='false';
        }else{
          errorState='';         
        } 
        
        if(contact.MailingPostalCode==null || contact.MailingPostalCode.length()==0){
          errorZip='Error: You must enter a Value';
             System.debug('In Validation...validating postal name');
          status='false';
        }else{
          errorZip='';         
        } 
        
        if(contact.MailingCountry==null || contact.MailingCountry.length()==0){
          errorCountry='Error: You must enter a Value';  
             System.debug('In Validation...validating country');
          status='false';      
        }else{
          errorCountry='';         
        } 
    
        return status;
      }
      
}