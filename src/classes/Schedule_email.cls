global class Schedule_email implements Schedulable
{
    global void execute(SchedulableContext ctx)
    {
        EmailMessage__c emass = new EmailMessage__c ();        
        list<EmailTemplate> lstEmailTemplates = [select Id,Name,DeveloperName from EmailTemplate where Name=: 'OutbondMessage'];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        if(lstEmailTemplates!=null && lstEmailTemplates.size()>0)
        mail.setTemplateId(lstEmailTemplates[0].Id);
        mail.setSaveAsActivity(false);
        mail.setToAddresses(new list<string>{'rao@gmail.com'});
        Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    }

 }