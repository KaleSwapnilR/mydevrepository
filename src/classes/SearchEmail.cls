global class SearchEmail implements Messaging.InboundEmailHandler {
      global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
      
      {
          Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
          Contact contact = new Contact();
          EmailMessage__c emass = new EmailMessage__c ();
          SearchApi searchApi = new SearchApi();
          Integer i = 0;          
          try
          {
                
                List<Contact> list2 = [select id from Contact where Email = :email.fromAddress];
                if(list2.isEmpty())
                {
                    contact.LastName = email.fromName;
                    System.debug('Contact Name ' +contact.LastName);
                    contact.Email = email.fromAddress;
                    System.debug('Email Address ' +contact.Email);
                    insert contact;
                    
                } //End of if it inserts contact if contact is not present in list 
               else 
                {
                    contact=list2.get(0);
                } // End of else contact will get contact from the list
                
                  emass.Subject__c = email.subject;
                  System.debug('Email Subject: '+emass.Subject__c );
                  emass.Body__c = email.plainTextBody ; 
                  System.debug('Email Body: '+emass.Body__c);
                  emass.contact__c =contact.id; 
       //           insert emass;              
                 
                  emass.Result__c = searchApi.api(emass.Subject__c);
               
                  insert emass;              
             
                                       
         /*         List<EmailMessage__c> list3 = [select id, Result__c from EmailMessage__c where Result__c  like :emass.Result__c];
                  System.debug('Email Address ' +contact.Email);
           
                  emass.Result__c = email.subject;                                                       
                  emass.contact__c =contact.id; 
                  insert emass;              
            
                   if(list3.isEmpty())
                       {
                          emass.Result__c = email.subject;                         
                          emass.contact__c =contact.id; 
                          insert emass;              
                          System.debug('Email result if not in list ' +EmailMessage__c.Result__c);                  
                       } 
                       
                       
                    else
                       {
                            for(i=0;i<list3.size();i++);
                            {
                                  
                                  EmailMessage__c em = list3.get(i);
                                  emass.Result__c  = EmailMessage__c.Result__c +'' + em.Result__c  ;
                                  emass.contact__c =contact.id; 
                                  insert emass;                    
                         //       System.debug('Email result if present ' +EmailMessage__c.Result__c); 
                            }
                       }  */  
              
              
               result.success = true ;
              
              
              
          }catch(Exception e)
          {
              result.success = false;
              System.debug('Query Issue: '+e);
              result.message = 'Oops, I failed.'+e;
          }
          return result; 
      }
  
 
}