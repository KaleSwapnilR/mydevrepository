public class ctrPipline {
    public Decimal intClosedWon {get; set;}
    public Decimal intClosedLost {get; set;}
    public Decimal intGate0 {get; set;}
    public Decimal intGate1 {get; set;}
    public Decimal intGate2 {get; set;}
    public Decimal intGate3 {get; set;}
    public Decimal intGate4 {get; set;}
    public Decimal intGate5 {get; set;}
    public Decimal intGate6 {get; set;}
    public Decimal intNego {get; set;} 
    public Decimal intMillionForm {get; set;}
    public list<AggregateResult> lstopp{get; set;} 
    public ctrPipline(){
      lstopp = [select count(id) total,stagename from Opportunity group by stagename];
      for(AggregateResult ar: lstopp){
            if(ar.get('stagename') == 'Closed Won') 
            {
             intClosedWon = integer.valueOf(ar.get('total'));
            } 
            if(ar.get('stagename') == 'Closed Lost')
            {
             intClosedLost = integer.valueOf(ar.get('total'));
            } 
            if(ar.get('stagename') == 'Prospecting')
            {
              intGate0 = integer.valueOf(ar.get('total'));
            }    
            if(ar.get('stagename') == 'Qualification')
            {
              intGate1 = integer.valueOf(ar.get('total'));
            }     
            if(ar.get('stagename') == 'Needs Analysis')
            {
              intGate2 = integer.valueOf(ar.get('total'));
            }     
            if(ar.get('stagename') == 'Value Proposition')
            {
              intGate3 = integer.valueOf(ar.get('total'));
            }   
            if(ar.get('stagename') == 'Id. Decision Makers')
            {
              intGate4 = integer.valueOf(ar.get('total'));
            } 
            if((ar.get('stagename') == 'Perception Analysis'))
            {
              intGate5 = integer.valueOf(ar.get('total'));
            }
            if((ar.get('stagename') == 'Proposal/Price Quote'))
            {
              intGate6 = integer.valueOf(ar.get('total'));
            }
            if((ar.get('stagename') == 'Negotiation/Review'))
            {
              intNego = integer.valueOf(ar.get('total'));
            }                        
       }
    } 
 }