public class newOpportunityCreationWizardController {

    public Boolean showPreview {get;set;}        //Show preview if user hits on preview buton;
    public String successMessage {get;set;}

    public List<List<Opportunity>> opportunityListToDownload{get;set;}


    public  List<String> excludeStages { get; set; }

    public String defaultStage { get; set; }
    public Boolean isDefaultStageSelected { get; set; }
    public Opportunity oppty{get;set;}     //get opportunity.

    public List<OpportunityRollOverMapping> listMapping {get;set;} // gets Mapping list of rules.
    public string currentShow{get;set;}    //get current show value.
    public string pastShow{get;set;}
    public String closeDateError {get;set;}
    public string currentShowError{get;set;} 

    public String ownerError {get;set;}
    public String mappingError{ get; set; }  //If no mapping is selected give an error.   
    public String defaultStageError { get; set; }    //If default stage not selected give an error.

    public Integer counter{get;set;}        //to count number of rules added.
    List<Opportunity> opportunityWithPastShow;
    List<Opportunity> opportunityWithCurrentShow;
    Map<String,String> mapFromAndToCriteria ;
    public List<OpportunityDataToShowPreview> opportunityDataToShowPreview {get;set;} // list to show details on vf page it 


    Map<String,OpportunityDataToShowPreview > previewMap ;

    
    public newOpportunityCreationWizardController(){
        opportunityListToDownload = new List<List<Opportunity>>();
        opportunityWithPastShow = new List<Opportunity>();
        opportunityWithCurrentShow = new List<Opportunity>();
        excludeStages =  new list<String>();
        oppty=new Opportunity(); 
        this.listMapping = new List<OpportunityRollOverMapping>();
        counter = 0;
        for(Integer i=0 ;i<3 ;i++)
        {
            addMappingRules();
        }
    }
    
    public PageReference printView() {

        PageReference oPageRef = Page.CreateCSVFileForOpprtunityRollOverWizard;
        oPageRef.setRedirect(false);
        system.debug('opportunityListToDownload '+opportunityListToDownload );


        List<Opportunity> temp = new List<Opportunity>();

        for(Integer i = 0; i < opportunityWithCurrentShow.size(); i++){
            if(temp.size() < 1000){
                temp.add(opportunityWithCurrentShow[i]);
            }
            else{
                opportunityListToDownload.add(temp);
                temp = new List<Opportunity>();
                temp.add(opportunityWithCurrentShow[i]);
            }
        }
        opportunityListToDownload .add(temp);


        return oPageRef;

    }

    public PageReference previewOpportunities() {
        System.debug('IN previewOpportunities**');

        previewMap = new Map<String,OpportunityDataToShowPreview >() ;


        if(validate() == True){
            opportunityWithPastShow = getOpportunitiesAccordingToExcludingCriteria();
            System.debug('opportunityWithPastShow '+opportunityWithPastShow );
            if(!opportunityWithPastShow.isEmpty()){
                opportunityWithCurrentShow = getOpportunitiesAccordingToRules(opportunityWithPastShow);
                System.debug('opportunityWithCurrentShow****** '+opportunityWithCurrentShow );

                for(Opportunity opp :opportunityWithCurrentShow ){
                    if(previewMap.ContainsKey(opp.StageName)==True){                    
                        previewMap.get(opp.StageName).totalAmount +=  opp.Amount ;
                        previewMap.get(opp.StageName).totalCount  += 1;
                        previewMap.get(opp.StageName).totalProbability += opp.Probability;

                        previewMap.get(opp.StageName).totalProbability = previewMap.get(opp.StageName).totalProbability / previewMap.get(opp.StageName).totalCount;
                        previewMap.get(opp.StageName).totalSalesPipeline = previewMap.get(opp.StageName).totalProbability * previewMap.get(opp.StageName).totalCount;


                    }
                    else
                    {
                        OpportunityDataToShowPreview previewData = new OpportunityDataToShowPreview(opp.StageName);
                        previewData .stage = opp.StageName ;
                        previewData .totalCount = 1 ;
                        previewData .totalAmount = opp.Amount;
                        previewData .totalProbability = opp.Probability;                      

                        previewData .totalSalesPipeline = previewData .totalProbability*1;  
                        previewMap.Put(opp.StageName,previewData); 
                    }
                    System.debug('previewMap*** '+previewMap );
                }
                System.debug('previewMap '+previewMap );
                opportunityDataToShowPreview = previewMap.values();
                showPreview = true ;
                saveRules();


            }
            else{
                currentShowError='There are no opportunities for this show';   
                showPreview = false ;         
            }
        }

        return null;
    }
        // get all the rules in a string format first and then get all opportunity according to the rule or criteria. 
    List<Opportunity> getOpportunitiesAccordingToRules(List <Opportunity> pastShowOpportunity){

        List<Show__c> listPastShow =[select id,Name from Show__c where Name=:pastShow];
        system.debug('pastShowOpportunity##'+pastShowOpportunity);
        List<Opportunity> listCurrentShowOppty=new List<Opportunity>();   
        mapFromAndToCriteria = new Map<String,String>();
        for(OpportunityRollOverMapping  opp : ListMapping){
            mapFromAndToCriteria.put(opp.fromStage,opp.toStage);
        }
        system.debug('**mapFromAndToCriteria '+mapFromAndToCriteria );
        if(isDefaultStageSelected){
            mapFromAndToCriteria.put('Default',defaultStage);
        }

        for(Opportunity opportunityWithPastShow : pastShowOpportunity)
        {
            string opptyOwner=opportunityWithPastShow.Owner.IsActive==true?opportunityWithPastShow.OwnerId:oppty.OwnerId;

            if(mapFromAndToCriteria.containsKey(opportunityWithPastShow.StageName)==true){

                Opportunity currentShowOppty= opportunityWithPastShow.clone(false,true,true,true);
                currentShowOppty.CloseDate = oppty.CloseDate;
                currentShowOppty.OwnerId=opptyOwner;
                currentShowOppty.Description ='Rolled over from '+pastShow;
                currentShowOppty.StageName = mapFromAndToCriteria.get(opportunityWithPastShow.StageName) ;
                currentShowOppty.Show__c = currentShow ;

                listCurrentShowOppty.add(currentShowOppty);

            }
            else{
                if(isDefaultStageSelected){

                    Opportunity currentShowOppty= opportunityWithPastShow.clone(false,true,true,true);
                    currentShowOppty.CloseDate = oppty.CloseDate;
                    currentShowOppty.OwnerId=opptyOwner;
                    currentShowOppty.Description ='Rolled over from '+pastShow;
                    currentShowOppty.StageName = mapFromAndToCriteria.get('Default') ;  
                    currentShowOppty.Show__c = currentShow ;                     
                    listCurrentShowOppty.add(currentShowOppty);                    
                }            
            }

            system.debug('**listCurrentShowOppty'+mapFromAndToCriteria );
            system.debug('**opportunityWithPastShow'+opportunityWithPastShow);

        }
        System.debug('listCurrentShowOppty##'+listCurrentShowOppty);   
        return listCurrentShowOppty;

    }

    // Get all opportunity according to the excluding rule or criteria. 
    List<Opportunity> getOpportunitiesAccordingToExcludingCriteria() {
        List<Opportunity> listOpportunityFromExcludingCriteria = new List<Opportunity>();
        system.debug('##excludeStages'+excludeStages );
        List<Show__c> listPastShow =[select id,Name from Show__c where Name=:pastShow];
        List<Show__c> listCurrentShow=[select id,Name from Show__c where id=:currentShow];

        if(!listPastShow.isEmpty() && !listCurrentShow.isEmpty() )
        {
            system.debug('##inIf'+excludeStages );
            listOpportunityFromExcludingCriteria  = [Select Id,Name,Amount,CloseDate,AccountId,StageName,Probability,Item_Type__c,TotalOpportunityQuantity,
                                                     OwnerId,Owner.IsActive from Opportunity where Show__c =: listPastShow[0].Id and StageName not in : excludeStages  ];        
        }
        else
        {
            listOpportunityFromExcludingCriteria  = [Select Id,Name,Amount,CloseDate,AccountId,StageName,Probability,Item_Type__c,TotalOpportunityQuantity,
                                                     OwnerId,Owner.IsActive from Opportunity where Show__c =: listPastShow[0].Id ];                        
        }

        return listOpportunityFromExcludingCriteria  ;
    }


    public PageReference cancel() {
         PageReference opportunityPage = new ApexPages.StandardController(oppty).view();
            opportunityPage.setRedirect(true);
            return opportunityPage; 
    }

    //Comman method for getting all shows.
    List <SelectOption> getShows()
    {
        List<Show__C> listOfShows =[select id,name from Show__c];   
        List<SelectOption> listOfShows_Select_Option=  new List<SelectOption>();
        listOfShows_Select_Option.add(new SelectOption('<Select>','<Select>'));   
        if(listOfShows.size()>0)
        {
            for(Show__c show:listOfShows)
            {
                listOfShows_Select_Option.add(new SelectOption(show.id,
                        show.Name));
            }
        }      
        return listOfShows_Select_Option;
    }
    
    public List <SelectOption> getCurrentShows() {
        List<Show__C> listOfShows =[select id,name from Show__c];           
        List<SelectOption> listOfCurrentShows=  new List<SelectOption>();
        if(listOfShows.size()>0)
        {
            for(Show__c show:listOfShows)
            {
                listOfCurrentShows.add(new SelectOption(show.id,
                        show.Name));
            }
        }       
        return listOfCurrentShows;
    }
    public List<SelectOption> getPastShows() {
        List<Show__C> listOfShows =[select id,name from Show__c];           
        List<SelectOption> listOfPastShows=  new List<SelectOption>();
        if(listOfShows.size()>0)
        {
            for(Show__c show:listOfShows)
            {
                listOfPastShows.add(new SelectOption(show.id,
                        show.Name));
            }
        }       
        return listOfPastShows;

    }
    
    //Get Opportunity satges.
    public List<selectoption> getOpptyStages() {
        List<selectoption> options = new List<selectoption>();
        //This is some generic code to retrieve the current stage names using
        //  Dynamic Apex in case the Stage Names change in the future
        Schema.DescribeFieldResult F = Opportunity.StageName.getDescribe();
        List<schema.picklistentry> typeList = F.getPicklistValues();

        for (Schema.PicklistEntry TypeItem : typeList){
            options.add(new SelectOption(TypeItem.getValue(),TypeItem.getLabel()));
        }

        return options;  

    }
    public PageReference addMappingRules() {

        List<SelectOption> listOfProbabilityStageMapping = new List<SelectOption>();
        listOfProbabilityStageMapping.add(new SelectOption('<Select>','<Select>'));
        OpportunityRollOverMapping opportunityRollOverMapping = new OpportunityRollOverMapping();
        counter++;
        opportunityRollOverMapping.counterWrap= counter;
        opportunityRollOverMapping.fromStage= '<Select>';
        opportunityRollOverMapping.toStage='<Select>';
        if(listMapping.size()<8){
            listMapping.add(opportunityRollOverMapping);
        }
        else{
            mappingError='Can not add more than 8 rules';
        }

        return null;
    }
    //Wrapper class 
    public class OpportunityRollOverMapping{
        Public Integer counterWrap{get;set;}
        public String fromStage{get;set;}
        public String toStage{get;set;}        
    }//End of wrapper class.

    

    public PageReference step1() {
        return Page.opptycreationwizardstep1;
    }

    public PageReference step2() {
        return Page.opptyCreationWizardStep2;
        return null;
    }
    
     public PageReference step3() {
        return Page.opptycreationwizardstep3;
    }
        //Remove the rule added to addMappingRow
    public pageReference removeMappingRow(){

        system.debug('**listMapping'+listMapping);
        system.debug('**counter'+listMapping);
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));

        for(Integer i=0;i<listMapping.size();i++){
            if(listMapping[i].counterWrap == param ){
                listMapping.remove(i);     
            }
        }


        counter--;

        return Null;
    }
    //class to create alist of records to show details of opportunity like Total count, Total amount, Total sales pipeline amount for every stages.
    public class OpportunityDataToShowPreview{
        public Integer totalCount { get; set; }
        public Decimal totalAmount { get; set; }
        public Decimal totalSalesPipeline {get;set;}
        public String stage {get;set;}
        public Decimal totalProbability {get;set;}

        public OpportunityDataToShowPreview(String Stage) {
            this.stage = stage ; 

        }
    }
    
        //valiadates all the required fields
    Boolean validate(){
        currentShowError= '' ;
        closeDateError ='';
        ownerError ='';
        mappingError = '';
        defaultStageError = '';
        Integer i=0,j=0;

        
        return true;
    }

    void saveRules(){
    }
    
    

}